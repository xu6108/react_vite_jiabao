export const incrementCount = {
  type: "increment",
  data: 1,
};

export const decrementCount = {
  type: "decrement",
  data: 1,
};
