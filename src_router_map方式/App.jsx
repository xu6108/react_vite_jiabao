import React from 'react'
import {useRoutes,Route,Routes} from "react-router-dom";
import { routes } from "@/routes";

export default function App() { 
  return (
    <Routes>
        {
          routes.map(item=>{
            return <Route key={item.path } path={item.path} element={item.element}>
                {item.children ? item.children.map(item=>{
                   return <Route key={item.path } path={item.path} element={item.element}></Route>
                }):""}
            </Route>
          })
        }
    </Routes>
  )
}
