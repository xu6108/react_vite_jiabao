import React from 'react'
import { useNavigate, Outlet } from 'react-router-dom'

export default function News() {
  const navigate = useNavigate();
  return (
    <div>
      <h3>News</h3>
      <div>
        <button onClick={() => { navigate("/center/news/detail?id=001&type=yule") }}>娱乐新闻</button>
        <button onClick={() => { navigate("/center/news/detail") }}>政治新闻</button>
        <button onClick={() => { navigate("/center/news/detail") }}>体育新闻</button>
      </div>
      <Outlet />
    </div>
  )
}
