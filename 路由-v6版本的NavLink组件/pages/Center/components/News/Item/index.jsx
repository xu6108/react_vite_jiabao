import React from 'react'
import { useSearchParams } from 'react-router-dom'

export default function Item() {
  /* 
    useSearchParams:
      - 可以获取到当前路径地址的查询字符串参数
      - 这个hooks返回一个数组,第一个值是查询字符串对象,第二个值是修改查询字符串的函数
      - 通过得到的查询字符串对象的get方法 读取对应的属性
  */
  const [searchParams, setSearchParams] = useSearchParams();
  // console.log(searchParams);
  // console.log(searchParams.get("id"));
  // console.log(searchParams.get("type"));
  const id = searchParams.get("id");
  const type = searchParams.get("type")

  console.log(1);
  return (
    <div>
      <h4>Item</h4>
      <p>id是:{id},类型是:{type}</p>
    </div>
  )
}
