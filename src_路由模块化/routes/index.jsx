import React from 'react'
import Music from "@/pages/Center/components/Music"
import News from "@/pages/Center/components/News"
import Game from "@/pages/Center/components/Game"
import Login from "@/pages/Login"
import Center from '@/pages/Center'
import NotFound from "@/pages/NotFound"
import { Navigate } from 'react-router-dom'

//路由表
export const routes = [
    {
        path:"/login",
        element:<Login></Login>
    },
    {
        path:"/center",
        element:<Center></Center>,
        children:[
            {
                path:"/center/music",
                element:<Music></Music>
            },{
                path:"/center/news",
                element:<News></News>
            },{
                path:"/center/game",
                element:<Game></Game>
            },{
                path:"/center/",
                element:<Navigate to="/center/game"></Navigate>
            },
        ]
    },
    {
        path:"/",
        element:<Navigate to="/center/music"></Navigate>
    },
    {
        path:"/*",
        element:<NotFound></NotFound>
    }
];