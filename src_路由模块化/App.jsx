import React from 'react'
import { Routes,Route } from 'react-router-dom'
import {routes} from "@/routes"

export default function App() { 
  return (
    <div>
      <Routes>
          {
            routes.map(item=>{
              return <Route key={item.path} path={item.path} element={item.element}>
                 {!item.children?"":item.children.map(item=>{
                  return <Route key={item.path} path={item.path} element={item.element}></Route>
                 })}
              </Route>
            })
          }
      </Routes>
        
    </div>
  )
}
