import React from 'react'
import store from '@/store/index'

export default function Count() {
  console.log("storestore-getstate",store.getState())
  return (
    <div>
      Count
      <p>count的值是{store.getState()}</p>
      <button onClick={()=>{
        store.dispatch({type:"inscreMent",data:1});
        console.log(store.getState())
      }}>累加1</button>
      <button onClick={()=>{
        store.dispatch({type:"delcreMent",data:1})
        console.log(store.getState())
      }}>累减1</button>
    </div>
  )
}