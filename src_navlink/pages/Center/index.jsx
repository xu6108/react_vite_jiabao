import React from 'react'
import { Outlet,NavLink } from 'react-router-dom'
import "./index.css"
export default function Center() {
  return (
    <div>
      <h1>center</h1>
      <NavLink to="/center/music" activeClassName="active"> 
          <button>跳转音乐</button>
       </NavLink>
      <NavLink to="/center/news"  activeClassName="active"> 
          <button>跳转新闻</button>
       </NavLink>
      <NavLink to="/center/game"  activeClassName="active"> 
          <button>跳转游戏</button>
       </NavLink>
      {/* 在一个组件的二级路由区域，是哟个Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
