import React from 'react'
// import { Outlet,Link } from 'react-router-dom'
import { Outlet,NavLink } from 'react-router-dom'
import "./index.css";

export default function Center() {
  return (
    <div>
      <h1>center</h1>
      <NavLink to="/center/music" activeClassName = "active">music </NavLink>
      <NavLink to="/center/news"  activeClassName = "active">news  </NavLink>
      <NavLink to="/center/game"  activeClassName = "active">game  </NavLink>
      {/* 在一个组件的二级路由区域，是哟个Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
