import React from 'react'
import {useRoutes,Route,Routes} from "react-router-dom";
import { routes } from "@/routes";

export default function App() { 

  const routeAddress = useRoutes(routes);
  return (
    <div>
      {routeAddress}
    </div>
  )
}
