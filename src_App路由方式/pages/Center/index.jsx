import React from 'react'
import { Outlet } from 'react-router-dom'
export default function Center() {
  return (
    <div>
      <h1>center</h1>
      {/* 在一个组件的二级路由区域，是哟个Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
