import React from 'react'
import Music from "@/pages/Center/components/Music"
import News from "@/pages/Center/components/News"
import Game from "@/pages/Center/components/Game"
import Login from "@/pages/Login"
import Center from '@/pages/Center'
import NotFound from "@/pages/NotFound"
import { Routes,Route,Navigate } from 'react-router-dom'

export default function App() { 
  return (
    <div>
        <Routes>
            <Route path='/login' element={<Login></Login>}></Route>
            <Route path='/center' element={<Center></Center>}>
                <Route path='/center/news' element={<News></News>}></Route>
                <Route path='/center/Game' element={<Game></Game>}></Route>
                <Route path='/center/Music' element={<Music></Music>}></Route>
            </Route>
            <Route path='/' element={<Login></Login>}></Route>
            <Route path='/*' element={<NotFound></NotFound>}></Route>
        </Routes>
    </div>
  )
}
