import { Navigate } from "react-router-dom";
import { lazy, Suspense } from "react"
import Loading from "@/components/Loading";
const Login = lazy(() => import("@/pages/Login"))
const Center = lazy(() => import("@/pages/Center"))
const NotFound = lazy(() => import("@/pages/NotFound"))
const Music = lazy(() => import("@/pages/Center/components/Music"))
const News = lazy(() => import("@/pages/Center/components/News"))
const NewsItem = lazy(() => import("@/pages/Center/components/News/Item"))
const Game = lazy(() => import("@/pages/Center/components/Game"))

const load = (comp) => {
  return (<Suspense fallback={<Loading />}>
    {comp}
  </Suspense>)
}
//路由表
export const routes = [
  {
    path: "/login",
    element: load(<Login />),
  },
  {
    path: "/center",
    element: load(<Center />),
    children: [
      {
        path: "/center",
        element: <Navigate to="/center/music" />
      },
      {
        path: "/center/music",
        element: load(<Music />),
      },
      {
        path: "/center/news",
        element: load(<News />),
        children: [
          {
            path: "/center/news/detail/:id?/:type?",
            element: load(<NewsItem />)
          }
        ]
      },
      {
        path: "/center/game",
        element: load(<Game />),
      },

    ],
  },
  {
    path: "/",
    element: <Navigate to="/center" />,
  },
  {
    path: "/*",
    element: load(<NotFound />),
  },
];




