import React from 'react'
import { routes } from "@/routes"
import { useRoutes } from "react-router-dom"

export default function App() {
  /* 
    Routes组件，主要是为了提供路由规则的书写区域
    Route组件，主要是书写每一个路由规则，path属性是代表当前路由的地址，element属性代表地址对应的组件
  
  */

  //一般来说 Hooks的书写位置只有两个地方1. 函数式组件内部  2. 自定义Hooks中
  const routerDOM = useRoutes(routes);

  return (
    <>
      {routerDOM}
    </>
  )
}
