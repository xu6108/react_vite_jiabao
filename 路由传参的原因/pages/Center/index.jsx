import React from 'react'
import { Outlet, NavLink, useNavigate } from 'react-router-dom'
import "./index.less"
export default function Center() {
  //useNavigate提供一个函数,函数内部可以直接书写路由地址,专门让我们进行编程式路由导航
  const navigate = useNavigate();

  return (
    <div>
      <h1>center</h1>
      <div>
        <NavLink to="/center/news" activeClassName="active">
          <button>news</button>
        </NavLink>
        <NavLink to="/center/music" activeClassName="active">
          <button>music</button>
        </NavLink>
        <NavLink to="/center/game" activeClassName="active">
          <button>game</button>
        </NavLink>

      </div>

      <div>
        <button onClick={() => { navigate("/center/news") }}>news</button>
        <button onClick={() => { navigate("/center/music") }}>music</button>
        <button onClick={() => { navigate("/center/game") }}>game</button>
      </div>

      <div>
        <button onClick={() => { navigate(1) }}>前进</button>
        <button onClick={() => { navigate(-1) }}>后退</button>
        <button onClick={() => { navigate(2) }}>前进2</button>
      </div>
      {/* 在一个组件的二级路由区域，使用Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
