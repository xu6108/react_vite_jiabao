import React from 'react'
import styles from "./index.module.less"
import { reqGitHubUserList } from "@/api"
import { useState } from "react";
export default function Header(props) {
  const {setUserList,setSearchStart,setIsSearching} = props;
  const [searchKey, setSearchKey] = useState("")

  //1. 表单数据双向绑定的事件回调函数
  const changeInput = (e) => {
    setSearchKey(e.target.value)
  }

  //2. 搜索按钮的事件回调函数
  const searchHandle = async () => {
    //判断输入的内容是否为空，如果为空，则不再向下执行
    if (!searchKey.trim()) {
      return alert("输入内容不能为空");
    }
    setSearchStart(true);

    setIsSearching(true);
    //发送请求
    const result = await reqGitHubUserList(searchKey);

    setUserList(result.items);

    setIsSearching(false);//拿到结果后将值修改 渲染列表
  }

  return (
    <div className={styles.header}>
      <h1>根据名字搜索github用户信息</h1>
      <div>
        <input type="text" value={searchKey} onChange={changeInput} />
        <button onClick={searchHandle}>搜索</button>
      </div>
    </div>
  )
}
