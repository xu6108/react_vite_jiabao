import React from 'react'
import Styles from "./index.module.less"
export default function List(props) {
  const {userList,searchStart,isSearching} = props;
  return (
    <ul className={Styles.outer}>
      {
        searchStart ? 
        isSearching ? "搜索中" : userList.map(item=>{
          return (
            <li key={item.id}>
              <img src={item.avatar_url} alt="" />
              <p>{item.login}</p>
            </li>
          )
        })
        : "请点击搜索按钮查询数据"
        
      }
    </ul>
  )
}

  