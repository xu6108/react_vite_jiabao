//配置axios第一步:引入axios
import axios from "axios";

//配置axios第二步:创建axios实例(创建一个axios的副本)
const request = axios.create({
  baseURL: "/path",
  timeout: 10000,
  headers: {},
});

//配置axios第三步:配置拦截器

//请求拦截器
request.interceptors.request.use(function (config) {
  return config;
});

//响应拦截器
request.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    return Promise.reject(error);
  }
);

//配置axios第四步:把当前的axios实例暴露出去
export default request;
