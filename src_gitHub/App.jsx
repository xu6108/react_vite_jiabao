import { useState } from "react";
import Header from "@/components/Headers"
import List from "@/components/List"
import "./App.less"
const App = () => {
  const [userList, setUserList] = useState([]);
  const [searchStart, setSearchStart] = useState(false);
  const [isSearching, setIsSearching] = useState(false);

  return (
    <div style={{ width: "808px", margin: "0 auto" }}>
      <Header setUserList={setUserList} setSearchStart={setSearchStart} setIsSearching={setIsSearching}/>
      <List userList={userList} searchStart={searchStart} isSearching={isSearching}/>
    </div>
  )
}

export default App