import request from "@/request/request";

export const reqGitHubUserList = (user = "")=>{
    return request.get(`/search/users?q=${user}`);
}