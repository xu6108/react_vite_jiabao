import React from 'react'
import { Outlet,Link } from 'react-router-dom'
export default function Center() {
  return (
    <div>
      <h1>center</h1>
      <Link to="/center/music">跳转音乐 </Link>
      <Link to="/center/news" >跳转新闻 </Link>
      <Link to="/center/game" >跳转游戏 </Link>
      {/* 在一个组件的二级路由区域，是哟个Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
