import React from 'react'
import store from "@/store"
import {getMovieList} from "@/store/actions/movieAction"

export default function Movie() {
  console.log(store.getState().movie.movieList)
  const {movieList} = store.getState().movie
  return (
    <div>Movie
      
        <h1>电影列表</h1>
        <p>count的数量是{store.getState().count}</p>
        <button onClick={() => { store.dispatch(getMovieList()) }}>获取按钮</button>
        <ul>
          {
            movieList.map(item=>{
              return (
                <li key={item.tvId}>
                  <h3>{item.name}</h3>
                  <p>
                    {item.description}
                  </p>
                </li>
              )
            })
          }
        </ul>
    </div>
  )
}
