import axios from "axios"
const getMovieAPI =
  "https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a";

export function getMovieList(){

    return async(dispatch)=>{
        const result = await axios.get(getMovieAPI);
        dispatch({
            type:"updateMovieList",
            data:result.data.data.list
        })
    }
}