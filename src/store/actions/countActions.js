//actions书写成对象则没有办法传递参数,所以我们可以把actions书写成一个函数,方便未来使用的时候传递参数
export const incrementCount = (num = 1) => ({
  type: "increment",
  data: num,
});

export const decrementCount = (num = 1) => ({
  type: "decrement",
  data: num,
});

//如果我们需要异步action的时候,action方法可以return一个函数,函数内部再执行异步
//当这个函数被组件内dispatch的时候,就会被一个中间件拦截住,然后调用函数,并传递一个dispatch作为参数
//让我们在中间件内执行完这个异步之后,然后再dispatch一个对象给store
export const incrementWaitCount = (num) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch({
        type: "increment",
        data: num,
      });
    }, 2000);
  };
};
