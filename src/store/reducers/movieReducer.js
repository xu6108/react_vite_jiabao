const initState = {
  movieList: [],
};

export default function movieReducer(preState = initState, action) {

  const {type,data} = action;
  // 第一种方式
  // switch(type){
  //   case "updateMovieList":
  //     return {...preState,movieList:data}
  // }

  // 第二种方式 redux中提供了immit方式 直接
  switch(type){
    case "updateMovieList":
        preState.movieList = data;
      break;
  }
  
  return preState;
}
