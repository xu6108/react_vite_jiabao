//1. 引入createStore方法创建一个 redux中心 store对象
import { createStore, combineReducers, applyMiddleware } from "redux";

import thunk from "redux-thunk";
//2. 引入即将创建的store对象的reducer
import countReducer from "./reducers/countReducer";
import movieReducer from "./reducers/movieReducer";

//combineReducers专门用来合并多个reducer,并给每一个reducer的数据起一个名字,方便未来使用
const allReducers = combineReducers({
  count: countReducer,
  movie: movieReducer,
});

// 3. 使用createStore方法创建一个store对象,并且引入当前store关联的reducer
//注意:第一次创建store并关联reducer的时候,就会调用一次reducer函数得到一个初始值
const store = createStore(allReducers, applyMiddleware(thunk));

console.log("store被执行了");

export default store;
