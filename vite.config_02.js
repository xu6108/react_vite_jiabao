import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve:{
    alias:{
      "@":path.resolve(__dirname,"./src")
    }
  },
  server:{
    hmr:true,
    proxy:{
      "/api":{
        target:"http://localhost:6108",
        changeOrigin:true,
        rewrite:(api)=>api.replace(/^\/api/,"")
      }
    }
  }
})