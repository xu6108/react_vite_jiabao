import React from 'react'
import {useRoutes } from 'react-router-dom'
import {routes} from "@/routes"

export default function App() { 
  const routesContent = useRoutes(routes);
  return (
    <div>
      {console.log(routesContent)}
      {routesContent}
    </div>
  )
}
