import React from 'react'
import "./index.less"
export default function Header(props) {
  const {insertSingle} = props;
  const keuupMethod = (e)=>{
    if(e.keyCode != 13){
      return;
    }

    const myvalue = e.target.value;
    if(!myvalue.trim()){
      return;
    }
    
    insertSingle({id:Date.now(),context:myvalue,done:false});

    e.target.value = "";
  }
  return (
    <div className="todo-header">
      <input type="text" placeholder="请输入你的任务名称，按回车键确认" onKeyUp={keuupMethod}/>
    </div>
  )
}
