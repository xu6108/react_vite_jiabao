import React from 'react'
import "./index.less"
import { useRef } from 'react';
export default function Footer(props) {
  const {todoList,allCheckState,setTodoList} = props;
  const changeBtn = ()=>{
    const newtodoList = todoList.map(item=>{return{...item,done:false}})
    setTodoList(newtodoList);
  }
  return (
    <div className="todo-footer">
      <label>
        <input type="checkbox" onChange={(e)=>{allCheckState(e.target.checked)}} checked={todoList.every(item=>item.done)} />
      </label>
      <span>
        <span>已完成{
          todoList.reduce((preVal,curVal)=>{
            return curVal.done ? preVal+1 : preVal
          },0)  
        }</span> / 全部{todoList.length}
      </span>
      <button className="btn btn-danger" onClick={changeBtn}>清除已完成任务</button>
    </div>
  )
}
