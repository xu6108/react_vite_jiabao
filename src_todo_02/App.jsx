import React from 'react'
import Header from "@/components/Headers"
import List from "@/components/List"
import Footer from "@/components/Footer"
import "./App.less"
import { reqTodoList } from "@/api"
import { useEffect,useState } from 'react'

export default function App() {

  const [todoList,setTodoList] = useState([]);
  //获取接口数据
  //async返回的是一个promise
  const reqTodoListFn =async ()=>{
    const result = await reqTodoList();
    setTodoList(result.data);
  }

  //初始化
  useEffect(()=>{
    reqTodoListFn();
  },[])
  
  const changeSingleType = (id)=>{
    const newtodoList = todoList.map(item=>{
      return item.id === id ? {...item,done:!item.done}  : item;
    })
    setTodoList(newtodoList);
  }

  const deleteSingle = (index)=>{
    todoList.splice(index,1);
    setTodoList([...todoList]);
  }
  
  const allCheckState = (state)=>{
    const newtodoList = todoList.map(item=>{
      return {...item,done:state}
    })

    setTodoList(newtodoList);
  }
  

  const insertSingle = (singleData)=>{
    setTodoList([singleData,...todoList])
  }

  return (
    <div className="todo-container">
      <div className="todo-wrap">
        <Header insertSingle={insertSingle} />
        <List todoList={todoList} deleteSingle={deleteSingle} changeSingleType={changeSingleType}/>
        <Footer todoList={todoList} allCheckState={allCheckState} setTodoList={setTodoList}/>
      </div>
    </div>
  )
}
