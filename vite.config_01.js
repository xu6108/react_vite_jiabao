import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from "path"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  //给内部的脚手架 配置解析
  resolve:{
    alias:{
      "@":path.resolve(__dirname,"./src")
    },
  },
  server:{
    hmr:true,
    proxy:{
      "/path":{
        target:"https://api.github.com/",
        changeOrigin:true,
        rewrite:path=>path.replace(/^\/path/,"")
      },
      "/api1":{
        target:"http://localhost:6108",
        changeOrigin:true,
        rewrite:path=>path.replace(/^\/api1/,"")
      }
    }
  }
})
