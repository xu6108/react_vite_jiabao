import { Navigate, useRoutes } from "react-router-dom";
import Login from "@/pages/Login";
import Center from "@/pages/Center";
import NotFound from "@/pages/NotFound";
import Music from "@/pages/Center/components/Music";
import News from "@/pages/Center/components/News";
import NewsItem from "@/pages/Center/components/News/Item";
import Game from "@/pages/Center/components/Game";

//路由表
export const routes = [
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/center",
    element: <Center />,
    children: [
      {
        path: "/center",
        element: <Navigate to="/center/music" />
      },
      {
        path: "/center/music",
        element: <Music />,
      },
      {
        path: "/center/news",
        element: <News />,
        children: [
          {
            path: "/center/news/detail/:id?/:type?",
            element: <NewsItem />
          }
        ]
      },
      {
        path: "/center/game",
        element: <Game />,
      },

    ],
  },
  {
    path: "/",
    element: <Navigate to="/center" />,
  },
  {
    path: "/*",
    element: <NotFound />,
  },
];




