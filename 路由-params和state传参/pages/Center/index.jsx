import React from 'react'
import { Outlet, NavLink, useNavigate } from 'react-router-dom'
import "./index.less"
export default function Center() {
  //useNavigate提供一个函数,函数内部可以直接书写路由地址,专门让我们进行编程式路由导航
  const navigate = useNavigate();

  return (
    <div>
      <h1>center</h1>
      <div>
        {/* 
          v5版本的旧写法
        */}
        {/* <NavLink to="/center/news" activeclassname="active">
          <button>news</button>
        </NavLink>
        <NavLink to="/center/music" activeclassname="active">
          <button>music</button>
        </NavLink>
        <NavLink to="/center/game" activeClassName="active">
          <button>game</button>
        </NavLink> */}

        {/* 
          v6版本的NavLink组件
            * 组件拥有一个className属性可以接受一个回调函数作为参数
            * 函数默认接受一个对象作为参数,对象内部有一个isActive的属性,代表当前的Nav是否被激活
            * 回调函数返回值就是当前className的值,所以可以在回调函数体中使用三元判断isActive决定是否给类名
        
        */}
        <NavLink to="/center/news" className={({ isActive }) => isActive ? "active" : ""}>
          <button>news</button>
        </NavLink>
        <NavLink to="/center/music" className={({ isActive }) => isActive ? "active" : ""}>
          <button>music</button>
        </NavLink>
        <NavLink to="/center/game" className={({ isActive }) => isActive ? "active" : ""}>
          <button>game</button>
        </NavLink>

      </div>

      <div>
        <button onClick={() => { navigate("/center/news") }}>news</button>
        <button onClick={() => { navigate("/center/music") }}>music</button>
        <button onClick={() => { navigate("/center/game") }}>game</button>
      </div>

      <div>
        <button onClick={() => { navigate(1) }}>前进</button>
        <button onClick={() => { navigate(-1) }}>后退</button>
        <button onClick={() => { navigate(2) }}>前进2</button>
      </div>
      {/* 在一个组件的二级路由区域，使用Outlet组件占位，未来一旦二级路由匹配，则把二级路由的组件替换Outlet组件 */}
      <Outlet />
    </div>
  )
}
