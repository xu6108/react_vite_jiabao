import React from 'react'
import { useNavigate, Outlet } from 'react-router-dom'

export default function News() {
  const navigate = useNavigate();
  return (
    <div>
      <h3>News</h3>
      <div>
        {/* 
          search传参,需要在地址后边拼接查询字符串
          在子组件中通过useSearchParams或者useLocation接受
        */}
        <button onClick={() => { navigate("/center/news/detail?id=001&type=yule") }}>娱乐新闻</button>
        {/* 
          params传参,需要在地址后边以地址的形式拼接数据,并且要在路由表中的地址上拼接 /:xx来接受对应的数据,如果某个数据是可选的则可以写`/:xx?`
          在子组件中通过useParams的方式接受
        */}
        <button onClick={() => { navigate("/center/news/detail/002/zhengzhi") }}>政治新闻</button>
        {/* 
          state传参：在navigate方法可以接收两个参数，第一个参数是to的地址，第二个参数是一个配置对象
            配置对象内部可以书写一个state属性，值是一个对象，代表传递给子组件的state参数
          在子组件中通过useLocation的方式接收
        */}
        <button onClick={() => {
          navigate("/center/news/detail", {
            state: {
              id: "003",
              type: "tiyu"
            }
          })
        }}>体育新闻</button>
      </div>
      <Outlet />
    </div>
  )
}
