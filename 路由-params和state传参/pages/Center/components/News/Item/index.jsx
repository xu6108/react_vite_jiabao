import React from 'react'
import { useLocation, useParams, useSearchParams } from 'react-router-dom'

export default function Item() {
  /* 
    useSearchParams:
      - 可以获取到当前路径地址的查询字符串参数
      - 这个hooks返回一个数组,第一个值是查询字符串对象,第二个值是修改查询字符串的函数
      - 通过得到的查询字符串对象的get方法 读取对应的属性
  */
  const [searchParams, setSearchParams] = useSearchParams();
  // console.log(searchParams);
  // console.log(searchParams.get("id"));
  // console.log(searchParams.get("type"));
  const id1 = searchParams.get("id");
  const type1 = searchParams.get("type")


  /* 
    useParams:
      * 可以拿到路由的params传参组成的对象
  
  */
  const params = useParams()
  console.log(params)
  const id2 = params.id;
  const type2 = params.type;


  /* 
    useLocation
  */
  const location = useLocation();
  console.log(location);
  const id3 = location.state?.id;
  const type3 = location.state?.type
  return (
    <div>
      <h4>Item</h4>
      <p>id是:{id1 || id2 || id3},类型是:{type1 || type2 || type3}</p>
    </div>
  )
}
