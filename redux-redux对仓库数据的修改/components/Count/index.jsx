import React from 'react'
import store from "@/store"
export default function Count() {
  console.log("在Count中打印获取的store", store);
  console.log("在Count中打印获取的store的值", store.getState());
  return (
    <div>
      <h1>Count组件</h1>
      <p>count的值是{store.getState()}</p>
      <div>
        <button onClick={() => {
          store.dispatch({ type: "increment", data: 1 });
          console.log(store.getState());
        }}>累加1</button>
        <button onClick={() => {
          store.dispatch({ type: "decrement", data: 1 });
          console.log(store.getState());
        }}>累减1</button>
      </div>
    </div>
  )
} 
