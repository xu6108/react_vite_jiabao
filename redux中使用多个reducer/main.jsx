import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import store from "@/store";
const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode >
    <App />
  </React.StrictMode >
);

//store对象提供了subscribe的方法,当store的数据更新的时候,subscribe接受的回调函数就会调用,我们可以重新渲染
store.subscribe(() => {
  root.render(<React.StrictMode >
    <App />
  </React.StrictMode >)
})


