import React from 'react'
import { useLocation } from 'react-router-dom'

export default function Item() {
  const location = useLocation();
  const result = location.search.slice(1).split("&").reduce((p, c) => {
    const [key, value] = c.split("=")
    p[key] = value;
    return p
  }, {})
  console.log(result);
  console.log('location ', location)
  return (
    <div>
      <h4>Item</h4>
      <p>id是:{result.id},类型是:{result.type}</p>
    </div>
  )
}
