import React from 'react'
import "./index.less"
export default function Footer(props) {
  const {todoList,settodoList,allCheckedState} = props;

  const changeClick = ()=>{
    const newtodoList = todoList.filter(item=>!item.done);
    settodoList(newtodoList);
  }


  return (
    <div className="todo-footer">
      <label>
        <input type="checkbox" onChange={ (e)=>{ allCheckedState(e.target.checked) } } checked={todoList.every(item=>item.done) && todoList.length != 0} />
      </label>
      <span>
        <span>已完成{
          todoList.reduce((preVal,curVal)=>{
            return curVal.done ? preVal+1 : preVal
          },0)  
        }</span> / 全部{todoList.length}
      </span>
      <button className="btn btn-danger" onClick={changeClick}>清除已完成任务</button>
    </div>
  )
}
