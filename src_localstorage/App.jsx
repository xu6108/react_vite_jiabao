import React from 'react'
import Header from "@/components/Headers"
import List from "@/components/List"
import Footer from "@/components/Footer"
import "./App.less"
import {reqtodoList} from "@/api"
import { useState,useEffect } from 'react'
import { getTodo,setTodo } from './utils/index';

export default function App() {
  const [todoList,settodoList] = useState(getTodo() || []);

  const reqtodoListFn = async()=>{
    const result = await reqtodoList();
    settodoList(result.data);
  }

  useEffect(()=>{
    // if(todoList.length === 0){
    //   reqtodoListFn();
    // }
    // todoList.length != 0 || reqtodoListFn();
    todoList.length === 0 && reqtodoListFn();
  },[])

  useEffect(()=>{
    setTodo(todoList);
  },[todoList]);

  const changeSingletype = (id)=>{

    const newtodolist = todoList.map(item=>{
      return item.id === id ? {...item,done:!item.done} : item;
    })
    settodoList(newtodolist);
  }

  const deleleSingle = (index)=>{
    todoList.splice(index,1);
    settodoList([...todoList]);
  }

  const allCheckedState = (type)=>{
    const newtodoList = todoList.map(item=>{return {...item,done:type}})
    settodoList(newtodoList);
  }

  const insertSingle = (singleData)=>{

    settodoList([singleData,...todoList]);
  }

  return (
    <div className="todo-container">
      <div className="todo-wrap">
        <Header insertSingle={insertSingle}/>
        <List todoList={todoList} changeSingletype={changeSingletype} deleleSingle={deleleSingle}/>
        <Footer todoList={todoList} settodoList={settodoList} allCheckedState={allCheckedState}/>
      </div>
    </div>
  )
}
