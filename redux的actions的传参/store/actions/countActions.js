//actions书写成对象则没有办法传递参数,所以我们可以把actions书写成一个函数,方便未来使用的时候传递参数
export const incrementCount = (num = 1) => ({
  type: "increment",
  data: num,
});

export const decrementCount = (num = 1) => ({
  type: "decrement",
  data: num,
});
