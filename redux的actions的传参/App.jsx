import React from 'react'
import Count from '@/components/Count'
import Movie from '@/components/Movie'
export default function App() {
  return (
    <div>
      <Count></Count>
      <hr />
      <Movie></Movie>
    </div>
  )
}
