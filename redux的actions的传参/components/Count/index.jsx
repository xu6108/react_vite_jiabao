import React from 'react'
import store from "@/store"
import { incrementCount, decrementCount } from "@/store/actions/countActions"
export default function Count() {
  console.log("在Count中打印获取的store", store);
  console.log("在Count中打印获取的store的值", store.getState());
  return (
    <div>
      <h1>Count组件</h1>
      <p>count的值是{store.getState().count}</p>
      <div>
        <button onClick={() => {
          store.dispatch(incrementCount());
        }}>累加1</button>
        <button onClick={() => {
          store.dispatch(decrementCount());
        }}>累减1</button>
        <button onClick={() => {
          store.dispatch(incrementCount(2));
        }}>累加2</button>
        <button onClick={() => {
          store.dispatch(decrementCount(2));
        }}>累减2</button>
      </div>
    </div>
  )
} 
