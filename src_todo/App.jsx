import React, { useState } from 'react'
import Header from "@/components/Headers"
import List from "@/components/List"
import Footer from "@/components/Footer"
import "./App.less"
import { reqTodoList } from "@/api"
import { useEffect } from 'react'

export default function App() {
  const [todoList,setTodoList] = useState([]);

  //列表渲染
  const todoListFn = async()=>{
    const result =  await reqTodoList();
    setTodoList(result.data);
  }

  useEffect(()=>{
    todoListFn();
  },[]);

  //修改状态
  const changeSingleType = (id)=>{
    const newtodoList = todoList.map(item=>{
      return item.id === id ? {...item,done:!item.done} : item;
    })
    setTodoList(newtodoList);
  }

  //删除
  const deleteSingleType = (index)=>{
    todoList.splice(index,1);
    setTodoList([...todoList]);
  }

  //全选
  const allChecked = (checkState)=>{
    const newtodoList = todoList.map(item=>{
      return {...item,done:checkState};
    })
    setTodoList(newtodoList);
  }

  //添加
  const insertSingleData = (newData)=>{

    setTodoList([newData,...todoList]);
  }

  return (
    <div className="todo-container">
      <div className="todo-wrap">
        <Header insertSingleData={insertSingleData}/>
        <List todoList={todoList} changeSingleType={changeSingleType} deleteSingleType={deleteSingleType}/>
        <Footer allChecked={allChecked} todoList={todoList} setTodoList={setTodoList}/>
      </div>
    </div>
  )
}
