import React from 'react'
import "./index.less"
import { useState } from 'react';

export default function Item(props) {

  const {id,context,done,index,changeSingleType,deleteSingleType} = props;

  const [mouseIndex, setMouseIndex] = useState(-1);

  return (
    <li className={index === mouseIndex ? "active" : ""} onMouseEnter={()=>{setMouseIndex(index)}} onMouseLeave={()=>{setMouseIndex(-1)}}>
      <label>
        <input type="checkbox" checked={done} onChange={(e)=>{changeSingleType(id,e.target.checked)}}/>
        <span>{context}</span>
      </label>
      <button className="btn btn-danger" onClick={()=>{deleteSingleType(index)}}>删除</button>
    </li>
  )
}
``