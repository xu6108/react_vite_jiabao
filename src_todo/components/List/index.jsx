import React from 'react'
import Item from "./components/Item"
import "./index.less"
export default function List(props) {
  const {todoList,changeSingleType,deleteSingleType} = props;
  return (
    <ul className="todo-main">
      {
        todoList.map((item,index)=>{
          return <Item key={item.id} {...item} index={index} changeSingleType={changeSingleType} deleteSingleType={deleteSingleType}/>
        })
      }
    </ul>
  )
}
