import React from 'react'
import "./index.less"
export default function Header(props) {
  const {insertSingleData} = props;
  const publish = (e)=>{
    if(e.keyCode != 13){//当keyCode不是回车键(13)时不执行
      return;
    }

    const myvalue = e.target.value;
    if(!myvalue.trim()){
      return;
    }

    insertSingleData({id:Date.now(),context:myvalue,done:false});

    e.target.value = "";
  }

  return (
    <div className="todo-header">
      <input type="text" placeholder="请输入你的任务名称，按回车键确认" onKeyUp={publish} />
    </div>
  )
}
