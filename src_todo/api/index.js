import request from "@/request/request";

export const reqTodoList = () => {
  return request.get("/getData");
};
