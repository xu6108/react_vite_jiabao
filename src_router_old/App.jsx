import React from 'react'
import {Routes,Route,Navigate} from "react-router-dom";
import Login from "@/pages/Login"
import Center from "@/pages/Center"
import NotFound from '@/pages/NotFound'
import Music from "@/pages/Center/components/Music"
import News from "@/pages/Center/components/News"
import Game from "@/pages/Center/components/Game"

export default function App() {
  return (
    <Routes>
        <Route path='/login' element={<Login/>}></Route>
        <Route path='/center' element={<Center/>}>
            <Route path="/center/music" element={<Music />}></Route>
            <Route path="/center/news" element={<News />}></Route>
            <Route path="/center/game" element={<Game />}></Route>
        </Route>
        
        <Route path='/' element={<Navigate to="/login" />}></Route>
        <Route path='/*' element={<NotFound></NotFound>}></Route>
    </Routes>
  )
}
