import React from 'react'
import { useLocation,useParams,useSearchParams } from 'react-router-dom'

export default function Item() {
  const useLocationObj = useLocation();
  // const [useSearch,setUseSearch] = useSearchParams();//第一个按钮的获取第二种方式
  // useSearch.get("id") useSearch.get("type") 
  const pnew = useLocationObj.search.slice(1).split("&").reduce((p,c)=>{//第一个按钮的获取第一种方式
    const [key,value] = c.split("=");
    p[key] = value;
    return p;
  },{})

  const useParams2 = useParams();// 第二个按钮

  // console.log(useLocationObj);


  return (
    <div>
      <h4>Item</h4>
      <p>id是:{pnew.id||useParams2.id||useLocationObj.state.id},类型是:{pnew.type||useParams2.type||useLocationObj.state.type}</p>
    </div>
  )
}
