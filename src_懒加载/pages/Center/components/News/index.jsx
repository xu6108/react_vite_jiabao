import React from 'react'
import {useNavigate,Outlet, useParams} from "react-router-dom"

export default function News() {
  const useNavigateFn = useNavigate();
  return (
    <div>News
      <button onClick={()=>{useNavigateFn("/center/news/detail?id=0001&type=tiyu")}}>体育新闻</button>
      <button onClick={()=>{useNavigateFn("/center/news/detail/0002/yule")}}>娱乐新闻</button>
      <button onClick={()=>{useNavigateFn("/center/news/detail/",{
        state:{
          id:"0003",
          type:"wanjian"
        }
      })}}>晚间新闻</button>
      <Outlet></Outlet>
    </div> 
  )
}
