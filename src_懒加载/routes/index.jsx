import {Suspense, lazy} from 'react'
// import Music from "@/pages/Center/components/Music"
// import News from "@/pages/Center/components/News"
// import Game from "@/pages/Center/components/Game"
// import Login from "@/pages/Login"
// import Center from '@/pages/Center'
// import NotFound from "@/pages/NotFound"
// import Newsitem from '@/pages/Center/components/News/Items'
import { Navigate } from 'react-router-dom'
import Loading from '@/components/index';

const Login = lazy(()=>import("@/pages/Login"));
const Center = lazy(()=>import("@/pages/Center"));
const NotFound = lazy(()=>import("@/pages/NotFound"));
const Music = lazy(()=>import("@/pages/Center/components/Music"));
const News = lazy(()=>import("@/pages/Center/components/News"));
const Game = lazy(()=>import("@/pages/Center/components/Game"));
const Newsitem = lazy(()=>import('@/pages/Center/components/News/Items'))

const load = (comp)=>{
    return (
        <Suspense fallback={<Loading/>}>
            {comp}
        </Suspense>
    );
}

//路由表
export const routes = [
    {
        path:"/login",
        element:load(<Login/>)
    },
    {
        path:"/center",
        element:load(<Center/>),
        children:[
            {
                path:"/center/music",
                element:load(<Music/>)
            },{
                path:"/center/news",
                element:load(<News/>),
                children:[
                    {
                        // path: "/center/news/detail",//第一种方式
                        path:"/center/news/detail/:id?/:type?",//第二种方式
                        element:load(<Newsitem/>)
                    }
                ]
            },{
                path:"/center/game",
                element:load(<Game/>)
            },{
                path:"/center/",
                element:<Navigate to="/center/game"></Navigate>
            },
        ]
    },
    {
        path:"/",
        element:<Navigate to="/center/music"></Navigate>
    },
    {
        path:"/*",
        element:load(<NotFound/>)
    }
];