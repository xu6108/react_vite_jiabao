//1. 引入createStore方法创建一个 redux中心 store对象
import { createStore } from "redux";

//2. 引入即将创建的store对象的reducer
import countReducer from "./reducer";

// 3. 使用createStore方法创建一个store对象,并且引入当前store关联的reducer
//注意:第一次创建store并关联reducer的时候,就会调用一次reducer函数得到一个初始值
const store = createStore(countReducer);

console.log("store被执行了");

export default store;
