//设置reducer的时候,给preState设置一个默认值,代表当前reducer的初始值
export default function CountRudecer(preState = 1, action) {
  console.log(preState, action);

  const { type, data } = action;

  switch (type) {
    case "increment":
      return preState + data;
    case "decrement":
      return preState - data;
  }

  return preState;
}
